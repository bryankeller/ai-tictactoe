//
//  GameViewController.swift
//  AI Tic Tac Toe
//
//  Created by Bryan Keller on 11/8/15.
//  Copyright © 2015 Bryan Keller. All rights reserved.
//

import UIKit

enum GameStatus {
  case Ended
  case InProgress
}

enum PlayerMode: Int {
  case UserControlled = 0
  case RandomPlacement = 1
  case BasicStrategy = 2
}

class GameViewController: UIViewController, TicTacToeBoardDelegate {

  @IBOutlet private weak var ticTacToeBoard: TicTacToeBoard!
  @IBOutlet private weak var player1WinCountLabel: UILabel!
  @IBOutlet private weak var player2WinCountLabel: UILabel!
  @IBOutlet private weak var drawCountLabel: UILabel!
  
  private var gameStatus = GameStatus.Ended
  private var isInteractiveMode: Bool {
    get {
      return self.player1Mode == .UserControlled || self.player2Mode == .UserControlled
    }
  }
  
  private let player1 = Player(markerImage: UIImage(named: "circle")!)
  var player1Mode = PlayerMode.UserControlled
  
  private let player2 = Player(markerImage: UIImage(named: "cross")!)
  var player2Mode = PlayerMode.BasicStrategy
  
  private var startingPlayer = 0
  private var numberOfGamesPlayed = 0
  private var player1WinCount = 0
  private var player2WinCount = 0
  private var drawCount = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.ticTacToeBoard.userInteractionEnabled = false
    self.ticTacToeBoard.delegate = self
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    self.startGame()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    self.ticTacToeBoard.delegate = nil
    self.gameStatus = .Ended
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  private func startGame() {
    self.startingPlayer += 1
    self.startingPlayer = self.startingPlayer % 2
    self.ticTacToeBoard.currentPlayer = self.startingPlayer == 0 ? self.player1 : self.player2
    
    ticTacToeBoard.clearBoard()
    
    self.gameStatus = .InProgress
    self.runGameTurn()
  }

  private func runGameTurn() {
    self.ticTacToeBoard.userInteractionEnabled = false
    
    self.ticTacToeBoard.currentPlayer = self.ticTacToeBoard.currentPlayer == self.player1 ? self.player2 : self.player1
    self.navigationItem.title = self.ticTacToeBoard.currentPlayer == self.player1 ? "Player 1's Turn" : "Player 2's Turn"
    if self.isInteractiveMode {
      self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: self.ticTacToeBoard.currentPlayer == self.player1 ? self.player1WinCountLabel.textColor : self.player2WinCountLabel.textColor]
    }
    
    let gameModeForCurrentPlayer = self.ticTacToeBoard.currentPlayer == self.player1 ? self.player1Mode : self.player2Mode
    if gameModeForCurrentPlayer == .BasicStrategy {
      if self.isInteractiveMode {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) {
          self.addMarkerUsingBasicStrategy()
        }
      }
      else {
        self.addMarkerUsingBasicStrategy()
      }
    }
    else if gameModeForCurrentPlayer == .RandomPlacement {
      if self.isInteractiveMode {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) {
          self.addMarkerUsingRandomPlacement()
        }
      }
      else {
        self.addMarkerUsingRandomPlacement()
      }
    }
    else if gameModeForCurrentPlayer == .UserControlled {
      self.ticTacToeBoard.userInteractionEnabled = true
    }
  }
  
  private func addMarkerUsingBasicStrategy() {
    let currentPlayer = self.ticTacToeBoard.currentPlayer
    let opponent = currentPlayer == self.player1 ? self.player2 : self.player1
    
    // Make 3 in a row if possible
    if let winningMoveCoordinate = self.ticTacToeBoard.winningMoveCoordinateForPlayer(currentPlayer) {
      if self.ticTacToeBoard.addMarker(winningMoveCoordinate) {
        self.log("Make 3 in a row if possible")
        return
      }
    }
    
    // Block opponent's 3 in a row if necessary
    if let winningMoveCoordinate = self.ticTacToeBoard.winningMoveCoordinateForPlayer(opponent) {
      if self.ticTacToeBoard.addMarker(winningMoveCoordinate) {
        self.log("Block opponent's 3 in a row if necessary")
        return
      }
    }
    
    // Make a fork if possible
    if let forkMoveCoordinate = self.ticTacToeBoard.forkMoveCoordinateForPlayer(currentPlayer) {
      if self.ticTacToeBoard.addMarker(forkMoveCoordinate) {
        self.log("Make a fork if possible")
        return
      }
    }
    
    // Block opponent's fork if necessary
    if let blockForkMoveCoordinate = self.ticTacToeBoard.forkMoveCoordinateForPlayer(opponent) {
      if self.ticTacToeBoard.addMarker(blockForkMoveCoordinate) {
        self.log("Block opponent's fork if necessary")
        return
      }
    }
    
    // Place a marker in the middle space if it's open
    if self.ticTacToeBoard.isMiddleOpen() {
      if self.ticTacToeBoard.addMarker(CGPoint(x: 1, y: 1)) {
        self.log("Place a marker in the middle space if it's open")
        return
      }
    }
    
    // Place a marker in a corner opposite the opponent
    if let oppositeCornerCoordinate = self.ticTacToeBoard.openCornerOppositeOpponentCoordinateForPlayer(currentPlayer) {
      if self.ticTacToeBoard.addMarker(oppositeCornerCoordinate) {
        self.log("Place a marker in a corner opposite the opponent")
        return
      }
    }
    
    // Place a marker in a corner space if one is open
    if let cornerCoordinate = self.ticTacToeBoard.openCornerCoordinate() {
      if self.ticTacToeBoard.addMarker(cornerCoordinate) {
        self.log("Place a marker in a corner space if one is open")
        return
      }
    }
    
    // Place a marker in a side space if one is open
    if let sideCoordinate = self.ticTacToeBoard.openSideCoordinate() {
      if self.ticTacToeBoard.addMarker(sideCoordinate) {
        self.log("Place a marker in a side space if one is open")
        return
      }
    }
    
    NSException(name:"Basic Strategy Failed", reason:nil, userInfo:nil).raise()
  }
  
  private func addMarkerUsingRandomPlacement() {
    var addedMarkerSuccessfully = false
    while (!addedMarkerSuccessfully) {
      addedMarkerSuccessfully = self.ticTacToeBoard.addMarker(CGPoint(x: Int(arc4random_uniform(3)), y: Int(arc4random_uniform(3))))
    }
  }
  
  private func log(message: String) {
    guard self.isInteractiveMode else {
      return
    }
    
    print(message)
  }
  
  
  // - MARK: TicTacToeBoardDelegate
  
  func ticTacToeBoard(ticTacToeBoard: TicTacToeBoard, didMarkBoardAtLocation location: CGPoint) {
    if self.gameStatus == .InProgress {
      self.runGameTurn()
    }
  }
  
  
  func ticTacToeBoard(ticTacToeBoard: TicTacToeBoard, gameEndedWithWinningPlayer winningPlayer: Player?) {
    self.gameStatus = .Ended
    self.numberOfGamesPlayed++
    
    let endOfGameText: String
    if winningPlayer == nil {
      self.drawCount++
      self.drawCountLabel.text = "Draw: \(self.drawCount)"
      endOfGameText = "The game ended in a draw."
    }
    else if winningPlayer == self.player1 {
      self.player1WinCount++
      self.player1WinCountLabel.text = "P1: \(self.player1WinCount) wins"
      endOfGameText = "Player 1 won!"
    }
    else {
      self.player2WinCount++
      self.player2WinCountLabel.text = "P2: \(self.player2WinCount) wins"
      endOfGameText = "Player 2 won!"
    }
    
    if self.isInteractiveMode {
      let alertController = UIAlertController(title: "Game Ended", message: endOfGameText, preferredStyle: .Alert)
      alertController.addAction(UIAlertAction(title: "Restart", style: .Default, handler: { (alertAction) -> Void in
        self.startGame()
      }))
      self.presentViewController(alertController, animated: true, completion: nil)
    }
    else {
      if self.numberOfGamesPlayed > 999 {
        self.navigationItem.title = "Game Ended"
        return
      }
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.startGame()
      })
    }
  }

}

