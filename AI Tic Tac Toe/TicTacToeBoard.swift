//
//  TicTacToeBoard.swift
//  AI Tic Tac Toe
//
//  Created by Bryan Keller on 11/8/15.
//  Copyright © 2015 Bryan Keller. All rights reserved.
//

import UIKit

protocol TicTacToeBoardDelegate: class {
  func ticTacToeBoard(ticTacToeBoard: TicTacToeBoard, didMarkBoardAtLocation location: CGPoint)
  func ticTacToeBoard(ticTacToeBoard: TicTacToeBoard, gameEndedWithWinningPlayer winningPlayer: Player?)
}

class TicTacToeBoard: UIView {
  
  private let winningConfigurationCoordinates = [
    // Horizontal winning plays
    [CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 0), CGPoint(x: 2, y: 0)],
    [CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 1), CGPoint(x: 2, y: 1)],
    [CGPoint(x: 0, y: 2), CGPoint(x: 1, y: 2), CGPoint(x: 2, y: 2)],
    
    // Vertical winning plays
    [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: 1), CGPoint(x: 0, y: 2)],
    [CGPoint(x: 1, y: 0), CGPoint(x: 1, y: 1), CGPoint(x: 1, y: 2)],
    [CGPoint(x: 2, y: 0), CGPoint(x: 2, y: 1), CGPoint(x: 2, y: 2)],
    
    // Diagonal winning plays
    [CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 1), CGPoint(x: 2, y: 2)],
    [CGPoint(x: 0, y: 2), CGPoint(x: 1, y: 1), CGPoint(x: 2, y: 0)]
  ]
  
  private let forkConfigurationCoordinates = [
    // Fork off middle
    [CGPoint(x: 1, y: 1), CGPoint(x: 1, y: 0), CGPoint(x: 0, y: 1)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 1, y: 0), CGPoint(x: 2, y: 1)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 1, y: 2), CGPoint(x: 0, y: 1)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 1, y: 2), CGPoint(x: 2, y: 1)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 0), CGPoint(x: 0, y: 2)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 0), CGPoint(x: 2, y: 0)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 2), CGPoint(x: 2, y: 2)],
    [CGPoint(x: 1, y: 1), CGPoint(x: 2, y: 0), CGPoint(x: 2, y: 2)],
    
    // Fork off corner
    [CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 0), CGPoint(x: 0, y: 1)],
    [CGPoint(x: 2, y: 0), CGPoint(x: 1, y: 0), CGPoint(x: 2, y: 1)],
    [CGPoint(x: 0, y: 2), CGPoint(x: 1, y: 2), CGPoint(x: 0, y: 1)],
    [CGPoint(x: 2, y: 2), CGPoint(x: 1, y: 2), CGPoint(x: 2, y: 1)],
    
    // Fork off side
    [CGPoint(x: 1, y: 0), CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 1, y: 0), CGPoint(x: 2, y: 0), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 0, y: 1), CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 0, y: 1), CGPoint(x: 0, y: 2), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 2, y: 1), CGPoint(x: 2, y: 0), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 2, y: 1), CGPoint(x: 2, y: 2), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 1, y: 2), CGPoint(x: 0, y: 2), CGPoint(x: 1, y: 1)],
    [CGPoint(x: 1, y: 2), CGPoint(x: 2, y: 2), CGPoint(x: 1, y: 1)]
  ]
  
  private var playerMarkerCoordinates = [(coordinate: CGPoint, markerImageView: UIImageView, player: Player)]()
  private var separatorLines = [UIView]()
  
  var currentPlayer: Player!
  
  weak var delegate: TicTacToeBoardDelegate?

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    self.commonInit()
  }
  
  private func commonInit() {
    for _ in 1...4 {
      let separatorLine = UIView()
      separatorLine.backgroundColor = UIColor.blackColor()
      self.separatorLines.append(separatorLine)
      self.addSubview(separatorLine)
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    // Layout separator lines
    self.separatorLines[0].frame = CGRect(x: self.bounds.size.width * (1/3), y: 0, width: 1, height: self.bounds.size.height)
    self.separatorLines[1].frame = CGRect(x: self.bounds.size.width * (2/3), y: 0, width: 1, height: self.bounds.size.height)
    self.separatorLines[2].frame = CGRect(x: 0, y: self.bounds.size.height * (1/3), width: self.bounds.size.width, height: 1)
    self.separatorLines[3].frame = CGRect(x: 0, y: self.bounds.size.height * (2/3), width: self.bounds.size.width, height: 1)
    
    for playerMarkerCoordinate in self.playerMarkerCoordinates {
      playerMarkerCoordinate.markerImageView.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width / 4, height: self.bounds.size.height / 4)
      let xWidth = self.bounds.size.width * (1/3)
      let yWidth = self.bounds.size.height * (1/3)
      playerMarkerCoordinate.markerImageView.center = CGPoint(x: CGFloat(playerMarkerCoordinate.coordinate.x) * xWidth + xWidth / 2, y: CGFloat(playerMarkerCoordinate.coordinate.y) * yWidth + yWidth / 2)
    }
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    guard let touch = touches.first where CGRectContainsPoint(self.bounds, touch.locationInView(self)) else {
      return
    }
    
    let xCellTouched = Int(touch.locationInView(self).x / self.bounds.size.width * 3)
    let yCellTouched = Int(touch.locationInView(self).y / self.bounds.size.height * 3)
    let boardCoordinate = CGPoint(x: xCellTouched, y: yCellTouched)
    
    self.addMarker(boardCoordinate)
  }
  
  func addMarker(coordinate: CGPoint) -> Bool {
    guard self.isCoordinateClear(coordinate) else {
      return false
    }
    
    let imageView = UIImageView(image: self.currentPlayer.markerImage)
    self.addSubview(imageView)
    self.playerMarkerCoordinates.append((coordinate: coordinate, markerImageView: imageView, player: self.currentPlayer))
    
    self.setNeedsLayout()
    
    if self.didPlayerWin(self.currentPlayer) {
      self.delegate?.ticTacToeBoard(self, gameEndedWithWinningPlayer: self.currentPlayer)
    }
    else if self.isBoardFull() {
      self.delegate?.ticTacToeBoard(self, gameEndedWithWinningPlayer: nil)
    }
    
    self.delegate?.ticTacToeBoard(self, didMarkBoardAtLocation: coordinate)
    
    return true
  }
  
  func clearBoard() {
    self.playerMarkerCoordinates.forEach { $0.markerImageView.removeFromSuperview() }
    self.playerMarkerCoordinates.removeAll()
    
    self.setNeedsLayout()
  }
  
  
  // MARK: - AI Helper methods
  
  // Returns true if the current player has three in a row
  private func didPlayerWin(player: Player) -> Bool {
    for winningPlayCoordinates in self.winningConfigurationCoordinates {
      if self.isPlayCompleteForPlayCoordinates(winningPlayCoordinates, player: player) {
        return true
      }
    }
    
    return false
  }
  
  // Returns true if the specified coordinate is unoccupied
  private func isCoordinateClear(coordinateToCheck: CGPoint) -> Bool {
    let coordinates = self.playerMarkerCoordinates.map { $0.coordinate }
    for coordinate in coordinates {
      if coordinateToCheck.x == coordinate.x && coordinateToCheck.y == coordinate.y {
        return false
      }
    }
    
    return true
  }
  
  // Returns true if all nine board spaces are occupied
  private func isBoardFull() -> Bool {
    return self.playerMarkerCoordinates.count == 9
  }
  
  // Returns the coordinate of a winning move space for the specified player, if one exists
  func winningMoveCoordinateForPlayer(player: Player) -> CGPoint? {
    var foundWinningMoveCoordinates = [CGPoint]()
    for winningPlayCoordinates in self.winningConfigurationCoordinates{
      if let foundWinningMoveCoordinate = self.availablePlayCompletingCoordinateForPlayCoordinates(winningPlayCoordinates, player: player) {
        foundWinningMoveCoordinates.append(foundWinningMoveCoordinate)
      }
    }
    
    return foundWinningMoveCoordinates.randomElement()
  }
  
  // Returns the coordinate of a fork move space for the specified player, if one exists
  func forkMoveCoordinateForPlayer(player: Player) -> CGPoint? {
    var foundforkCoordinatesForPlayer = [CGPoint]()
    for forkPlayCoordinates in self.forkConfigurationCoordinates {
      if let foundforkCoordinateForPlayer = self.availablePlayCompletingCoordinateForPlayCoordinates(forkPlayCoordinates, player: player) {
        foundforkCoordinatesForPlayer.append(foundforkCoordinateForPlayer)
      }
    }
    
    return foundforkCoordinatesForPlayer.randomElement()
  }
  
  // Returns true if the middle is open
  func isMiddleOpen() -> Bool {
    return self.playerMarkerCoordinates.filter { $0.coordinate.x == 1 && $0.coordinate.y == 1 }.count == 0
  }
  
  // Returns a coordinate for an open corner opposite the specified player's opponent's corner marker, if one exists
  func openCornerOppositeOpponentCoordinateForPlayer(player: Player) -> CGPoint? {
    let cornerCoordinates = [CGPoint(x: 0, y: 0), CGPoint(x: 2, y: 0), CGPoint(x: 0, y: 2), CGPoint(x: 2, y: 2)]
    let opponentCornerMarkerCoordinates = self.playerMarkerCoordinates.filter { $0.player != player }.map { $0.coordinate }.filter { cornerCoordinates.contains($0) }
    for opponentCornerMarkerCoordinate in opponentCornerMarkerCoordinates {
      let oppositeCornerCoordinate = CGPoint(x: abs(opponentCornerMarkerCoordinate.x - 2), y: abs(opponentCornerMarkerCoordinate.y - 2))
      if self.markerCoordinatesForPlayer(nil).contains(oppositeCornerCoordinate) == false {
        return oppositeCornerCoordinate
      }
    }
    
    return nil
  }
  
  // Returns the coordinate of the first found open corner space
  func openCornerCoordinate() -> CGPoint? {
    let cornerCoordinates = [CGPoint(x: 0, y: 0), CGPoint(x: 2, y: 0), CGPoint(x: 0, y: 2), CGPoint(x: 2, y: 2)]
    return cornerCoordinates.filter { self.markerCoordinatesForPlayer(nil).contains($0) == false }.randomElement()
  }
  
  // Returns the coordinate of the first found open side space
  func openSideCoordinate() -> CGPoint? {
    let sideCoordinates = [CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 0), CGPoint(x: 1, y: 2), CGPoint(x: 2, y: 1)]
    return sideCoordinates.filter { self.markerCoordinatesForPlayer(nil).contains($0) == false }.randomElement()
  }
  
  // Returns all marker coordinates for the specified player
  private func markerCoordinatesForPlayer(player: Player?) -> [CGPoint] {
    return self.playerMarkerCoordinates.filter { $0.player == player || player == nil }.map { $0.coordinate }
  }
  
  // Returns the coordinate that will complete the specified play for the specified player, if it's open
  private func availablePlayCompletingCoordinateForPlayCoordinates(playCoordinates: [CGPoint], player: Player) -> CGPoint? {
    let playCompletingCoordinates = playCoordinates.filter { self.markerCoordinatesForPlayer(player).contains($0) == false }
    guard let playCompletingCoordinate = playCompletingCoordinates.randomElement() where playCompletingCoordinates.count == 1 else {
      return nil
    }
    
    guard self.markerCoordinatesForPlayer(nil).contains(playCompletingCoordinate) == false else {
      return nil
    }
    
    return playCompletingCoordinate
  }
  
  // Returns true if the specified play is complete for the specified player
  private func isPlayCompleteForPlayCoordinates(playCoordinates: [CGPoint], player: Player) -> Bool {
    return playCoordinates.filter { self.markerCoordinatesForPlayer(player).contains($0) == true }.count == 3
  }
  
}
