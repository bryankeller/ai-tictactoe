//
//  ConfigViewController.swift
//  AI Tic Tac Toe
//
//  Created by Bryan Keller on 12/10/15.
//  Copyright © 2015 Bryan Keller. All rights reserved.
//

import UIKit

class ConfigViewController: UIViewController {

  @IBOutlet private weak var player1GameModeSegmentedControl: UISegmentedControl!
  @IBOutlet private weak var player2GameModeSegmentedControl: UISegmentedControl!
 
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let destinationViewController = segue.destinationViewController as? GameViewController else {
      return
    }
    
    destinationViewController.player1Mode = PlayerMode(rawValue: self.player1GameModeSegmentedControl.selectedSegmentIndex)!
    destinationViewController.player2Mode = PlayerMode(rawValue: self.player2GameModeSegmentedControl.selectedSegmentIndex)!
  }
  
}
