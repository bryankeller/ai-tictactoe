//
//  Player.swift
//  AI Tic Tac Toe
//
//  Created by Bryan Keller on 11/9/15.
//  Copyright © 2015 Bryan Keller. All rights reserved.
//

import UIKit

class Player: NSObject {

  let markerImage: UIImage
  
  init(markerImage: UIImage) {
    self.markerImage = markerImage
  }
  
}
