//
//  Array+RandomElement.swift
//  AI Tic Tac Toe
//
//  Created by Bryan Keller on 12/9/15.
//  Copyright © 2015 Bryan Keller. All rights reserved.
//

import Foundation

extension Array {
  
  func randomElement() -> Element? {
    guard self.count > 0 else {
      return nil
    }
    
    let randomIndex = Int(arc4random_uniform(UInt32(self.count)))
    return self[randomIndex]
  }
  
}